
\version "2.18.2"
% automatically converted by musicxml2ly from Du_warest_allzeit_Herr_unsere_Zuflucht.xml

%% additional definitions required by the score:
\language "deutsch"

#(set-global-staff-size 18.5)

\paper {
	#(set-paper-size "a4")
%	indent = 0
%	system-system-spacing #'basic-distance = #24
}

%
\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

\header {
    encodingsoftware = "MuseScore 2.0.2"
    encodingdate = "2018-11-04"
    composer = "Jan Pieterszoon Sweelinck (1562 - 1621)"
    title = "Du warest allzeit Herr unsere Zuflucht"
    subtitle = "Pseaumes de David, Livre 2"
%    subsubtitle = ""
%    opus = ""
    poet = "Psalm 90"
	copyright = "© Volker Seibt 2018 (Lizenz CC BY-SA 3.0 DE)"
	% Voreingestellte LilyPond-Tagline entfernen
	tagline = ##f
}

Cantus =  \relative a' {
    \clef "treble" \key c \major \time 4/2 a\breve
    | % 2
    a\breve | % 3
    d1 c4 a4 c4 c4 | % 4
    h4 a4 gis2 a2 r2 | % 5
    e'2 d4 c4 h4 a4 r4 c4 | % 6
    h4 a4 gis2 a2 r4 e'4 | % 7
    c4 a4 e'4 c4 a4 f4 c'4 a4 | % 8
    f4 d4 a'1 a4 c4 | % 9
    h2 a4 d4 d4 (cis8 h8 cis2) | \barNumberCheck #10
    d1 r2 a2 | % 11
    b2 g2 a2 f2 | % 12
    g2 a2 d,2 f2 | % 13
    e1 d2 a'2 ~ | % 14
    a2 h4 cis4 d2 a4 d4 | % 15
    d2 c4 c4 h1 | % 16
    a\breve ~ | % 17
    a\breve | % 18
    r2 r4 a4. g8 a4 f4 d4 | % 19
    a'2 b2 a2 g2 | \barNumberCheck #20
    f4 a4 d4 c4 a2 r4 a4 | % 21
    d4 c4 a1 r4 e4 | % 22
    f4 e4 c4 a'2 a4 f2 | % 23
    a2 g2 f2 f4 e4 | % 24
    d2 r2 r2 a'2 ~ | % 25
    a4 d2 c4 a1 | % 26
    f2. g4 d1 | % 27
    f4 g4 d2 a'2. a4 | % 28
    fis2 a1 g2 | % 29
    fis\breve \bar "|."
}

CantusLyrics = \lyricmode {
	Tu __ as __ es -- té, Sei -- gneur, no -- stre re -- trai -- cte, __
	no -- stre re -- trai -- cte, no -- stre re -- trai -- cte,
	et seur re -- cours, et seur re -- cours, et seur re -- cours __
	de li -- gne' en li -- gne -- e: __
	Me -- mes de -- vant nul -- le mon -- ta -- gne __ ne -- e,
	et __ que le mond' et la ter -- re fust fai -- cte, __
	tu es tois Dieu des -- ja __ com -- me __ tu __ es,
	et comm' aus -- si, et comm' aus -- si, et comm' aus -- si
	tu se -- ras à __ ja -- mais, à ja -- mais, __
	et __ comm' aus -- si __ tu __ se -- ras, tu se -- ras à __ ja -- mais, à __ ja -- mais. __
}

Altus =  \relative a {
    \clef "treble" \key c \major \time 4/2 a2 a4 d4
    c4 a4 c4 f4 | % 2
    e4 c4 f2 e8 (d8 c8 h8 a2) | % 3
    r2 r4 h4 e2. e4 | % 4
    d4 c4 h2 a2 r2 | % 5
    r4 c4 h4 a4 gis4 a4 r2 | % 6
    r2 e'1 c2 | % 7
    a2. e'4 r4 a4 f2 | % 8
    r4 a4 f4 d4 e4. (d8 c4) g'4 ~ | % 9
    g4 g4 fis4 g8 g8 a1 | \barNumberCheck #10
    a2 a,1 f'2 | % 11
    d2 e2 c2 d2 | % 12
    e2 a,4 c4 h2 a2 | % 13
    r2 a'2. (g4 fis4 e4 | % 14
    fis2) g4 g4 fis2 fis4 a4 | % 15
    g2 e4 e4 e2 e4 e4 ~ | % 16
    e8 d8 e4 c4 a4 e'1 | % 17
    f2 e2 d2 cis2 | % 18
    r2 f2 e2 d2 | % 19
    c8 (d8 e8 f8 g2) r4 c,4 d4 e4 | \barNumberCheck #20
    f1 r4 a,4 d4 c4 | % 21
    a2 r4 c4 f4 e4 c2 | % 22
    r2 r4 c2 d4 a2 | % 23
    f'4 e4 d2 a'1 ~ | % 24
    a2 g2 fis1 | % 25
    d2 a'2 f2 e2 | % 26
    d2. e4 f2 r2 | % 27
    r2 f4 g4 c,4 d2 cis4 | % 28
    d\breve ~ | % 29
    d\breve \bar "|."
}

AltusLyrics = \lyricmode {
	Tu __ as es -- té, Sei -- gneur, no -- stre re -- trai -- cte, __
	Sei -- gneur, __ no -- stre re -- trai -- cte, __ no -- stre re -- trai -- cte,
	et __ seur __ re -- cours, et seur, __ et seur re -- cours, __
	de li -- gne' en li -- gne -- e:
	Me -- mes de -- vant nul -- le mon -- ta -- gne ne -- e, __
	et __ que le mond' et la ter -- re fust fai -- cte, __
	tu es tois Dieu des -- ja __ com -- me __ tu __ es, __
	com -- me __ tu __ es, __ com -- me tu es, __ et comm' aus -- si, __
	et comm' aus -- si __ tu se -- ras __ à ja -- mais, __ à __ ja -- mais, __
	et __ comm' __ aus -- si __ tu __ se -- ras, __ tu se -- ras à ja -- mais. __
}

Tenor =  \relative a {
    \clef "treble_8" \key c \major \time 4/2 R1*2 | % 2
    r4 a4 a4 d4 c4 a4 c4 c4 | % 3
    h4 a4 gis2 a1 | % 4
    r2 r4 e'4 e4 e4 d4 c4 | % 5
    h4 a4 r4 e'4 e2 d4 e4 ~ | % 6
    e4 c4 h2 a2 e'2 ~ | % 7
    e2 c1 a2 | % 8
    d2 d2 c4 a4 e'4 e4 ~ | % 9
    e4 d4 d4 d8 e8 f4 e8 (d8 e2 | \barNumberCheck #10
    fis1) r4 a,2 d4 ~ | % 11
    d4 g,2 c2 a2 f4 | % 12
    c'2 c4 a4 r4 d4. c8 (a8 h8 | % 13
    cis4 d2 cis4 d1) | % 14
    d2 d4 e4 a,2 d4 d4 | % 15
    h2 a4 a4 gis4 a2 (gis4 | % 16
    a1) r4 a4. g8 a4 | % 17
    f4 d4 a'2 f'2 e2 ~ | % 18
    e2 d2 cis2 r2 | % 19
    r4 c4 d4 e4 f2 r2 | \barNumberCheck #20
    r2 r4 a,4 d4 c4 a2 | % 21
    r2 r4 a4 d4 c4 a2 ~ | % 22
    a1 r2 c4 d4 | % 23
    a4 c2 b4 a2 a4 a4 | % 24
    d,4 d'2 cis4 d1 | % 25
    r2 r4 a2 d2 c4 | % 26
    a2 r2 r2 f4 g4 | % 27
    d1 f2 e2 | % 28
    d2 fis4 g4 a2 b4 b4 | % 29
    a\breve \bar "|."
}

TenorLyrics = \lyricmode {
	Tu as es -- té, Sei -- gneur, no -- stre re -- trai -- cte, __
	Sei -- gneur, no -- stre re -- trai -- cte, Sei -- gneur, __ no -- stre re -- trai -- cte,
	et __ seur __ re -- cours, __ et __ seur re -- cours de __ lig -- ne' en li -- gne -- e: __
	Mes -- mes __ de -- vant nul -- le mon -- tag -- ne ne -- e, __
	et que le mond' et la ter -- re fust fai -- cte, __
	tu es tois Dieu des -- ja com -- me __ tu __ es, __ com -- me tu es, __
	et comm' aus -- si, __ et comm' aus -- si __ tu se -- ras à __ ja -- mais,
	tu se -- ras à __ ja -- mais, __
	et __  comm' __ aus -- si __ tu se -- ras __ à __ ja -- mais, tu se -- ras à ja -- mais. __
}

Bassus =  \relative a, {
    \clef "bass" \key c \major \time 4/2 r1 a2 a4 d4
    | % 2
    c4 a4 d8 (e8 f8 g8 a4. g8 f4) e4 | % 3
    d4 c4 h2 a1 | % 4
    r2 e'2 c'4 c4 h4 a4 | % 5
    gis4 a4 r2 r4 c4 h4 a4 | % 6
    gis4 (a4 e2) a,1 | % 7
    r2 a'2 f1 | % 8
    d1 a'2 a4 e4 | % 9
    g2 d4 b'4 a1 | \barNumberCheck #10
    d,1 r2 d2 | % 11
    g2 c,2 f2 d2 | % 12
    c2 f2 g4 d4 d8 (e8 f8 g8 | % 13
    a2) a,2 r2 d2 ~ | % 14
    d2 g4 e4 d2 d4 fis4 | % 15
    g2 a4 a,4 e'1 | % 16
    a,1. r2 | % 17
    r1 r2 r4 a'4 ~ | % 18
    a8 g8 a4 f4 d4 a'2 b2 | % 19
    a2 g2 f4 a4 b4 c4 | \barNumberCheck #20
    f,\breve ~ | % 21
    f1 r2 r4 a,4 | % 22
    d4 c4 a2 f'1 ~ | % 23
    f2 g2 d1 | % 24
    f2 e2 d1 | % 25
    r1 d2 a'4 a,4 | % 26
    d2 d4 c4 b1 | % 27
    b1 a1 | % 28
    d2 d4 e4 fis2 g4 g,4 | % 29
    d'\breve \bar "|."
}

BassusLyrics = \lyricmode {
	Tu __ as es -- té, Sei -- gneur, __ no -- stre re -- trai -- cte, __
	Sei -- gneur, no -- stre re -- trai -- cte, no -- stre re -- trai -- cte, __
	et __ seur __ re -- cours __ de li -- gne' __ en li -- gne -- e: __
	Mes -- mes de -- vant nu -- le mon -- tag -- ne ne -- e, __
	et __ que le mond' et la ter -- re fust fai -- cte, __
	tu __ es tois Dieu des -- ja __ com -- me __ tu __ es,
	com -- me tu es, __
	et comm' aus -- si __ tu __ se -- ras __ à __ ja -- mais, __
	et __ comm' aus -- si tu se -- ras __ à __ ja -- mais,
	tu se -- ras à ja -- mais. __
}

% The score definition
\score {
    \new ChoirStaff
    	<<

			\new Staff \with {
				instrumentName = "Cantus"
			} { \Cantus }
			\addlyrics { \CantusLyrics }

			\new Staff \with {
				instrumentName = "Altus"
			} { \Altus }
			\addlyrics { \AltusLyrics }

			\new Staff \with {
				instrumentName = "Tenor"
			} { \Tenor }
			\addlyrics { \TenorLyrics }

			\new Staff \with {
				instrumentName = "Bassus"
			} { \Bassus }
			\addlyrics { \BassusLyrics }

        >>
    \layout {}
    \midi {}
    }

