
\version "2.18.2"

%% additional definitions required by the score:
\language "deutsch"

#(set-global-staff-size 17.5)

\paper {
	#(set-paper-size "a4")
	indent = 0
}

\header {
	title = "Erlaube mir, feins Mädchen"
	composer = "Johannes Brahms, 1833 -1897"
	subtitle = "aus \"12 Deutsche Volkslieder\""
	opus = "WoO posthum 35, Nr. 3"
	copyright = "© Volker Seibt 2018 (Lizenz CC BY-SA 3.0 DE)"
	% Voreingestellte LilyPond-Tagline entfernen
	tagline = ##f
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

PartPOneVoiceOne =  \relative f' {
    \clef "treble" \key f \major \time 3/4
    \mergeDifferentlyDottedOn
    \phrasingSlurDashed
    \partial 4 f4 \p ^\markup{\bold {Zart} } | % 1
    f8 \(a8\) a4. b8 | % 2
    a8 (g8) g4 f8 \(f8\) | % 3
    e4 e8 [(f8)] g8 [(a8)] | % 4
    f4 r4 c4 | % 5
    f8 (a8) a4. b8 | % 6
	<< { \voiceOne a8_(g8) g4 f8 f8 }
    \new Voice { \voiceTwo s4 g4. f8 } | % 7
	>> \oneVoice
    e4 e8 [(f8)] g8 [(a8)] | % 8
    f4 r4 c'4 | % 9
    c8 \(a8\) a4. c8 | \barNumberCheck #10
    c8 (g8) g4. c8 | % 11
    h8 \< \(c8\) d4 e4 \! | % 12
    c4 \> r4 \! c,8 c8 | % 13
	<< { \voiceOne f8_(a8) a4 b8 b8 }
    \new Voice { \voiceTwo s4 a4. b8 }
    >> \oneVoice | % 14
    a8 (g8) g4. f8 | % 15
    e8 \(f8\) g4 a4 | % 16
    f4 r4 \bar "|."
}

PartPOneVoiceOneLyricsOne =  \lyricmode {
	\set stanza = #"1."
	Er -- lau -- be mir, feins Mäd -- chen,
	in den Gar -- ten  __ zu __ gehn,
    daß ich __ mag dort schau -- en,
    wie die Ro -- sen __ so __ schön.
    Er -- lau -- be sie zu bre -- chen,
    es ist die höch -- ste Zeit,
    ih -- re Schön -- heit, ih -- re Ju -- gend
    hat mir mein Herz er -- freut.
}

PartPOneVoiceOneLyricsTwo =  \lyricmode {
	\set stanza = #"2."
	O Mäd -- _ chen, o Mäd -- chen,
	du __ _ ein -- sa -- mes __ Kind,
	wer hat __ den Ge -- dan -- ken __ _
    ins Herz dir __ ge -- zinnt,
    daß ich _ soll den Gar -- ten die Ro -- _ sen nicht sehn?
    du ge -- fällst __ mei -- _ nen Au -- gen,
    das muß __ _ ich ge -- stehn.
}

PartPTwoVoiceOne =  \relative c' {
    \clef "treble" \key f \major \time 3/4
    \mergeDifferentlyDottedOn
    \phrasingSlurDashed
    \partial 4 c4 \p | % 1
    c8 \(f8\) f4. f8 | % 2
	<< { \voiceOne f4 f4 f8 c8 }
    \new Voice { \voiceTwo s2 r8 c8 } | % 3
	>> \oneVoice
    c4 c4 e4 | % 4
    f4 r4 c4 | % 5
    c8 (f8) f4. f8 | % 6
	<< { \voiceOne f8_(d8) d4 c8 c8 }
    \new Voice { \voiceTwo s4 d4. c8 } | % 7
	>> \oneVoice
    c4 c4 e4 | % 8
    f4 r4 f4 | % 9
    a8 \(f8\) f4. a8 | \barNumberCheck #10
    g4 g4. g8 | % 11
    g8 \< \(c8\) h4 h4 \! | % 12
    c4 \> r4 \! c,8 c8 | % 13
    c4 f4 f8 f8 | % 14
    f8 (d8) d4. d8 | % 15
    c8 \(c8\) d4 e4 | % 16
    c4 r4 \bar "|."
}

PartPThreeVoiceOne =  \relative a {
    \clef "treble_8" \key f \major \time 3/4
    \mergeDifferentlyDottedOn
    \phrasingSlurDashed
    \partial 4 a4 \p | % 1
    a8 \(c8\) c4. d8 | % 2
    c8 (b8) b4 a8 \(a8\) | % 3
    g4 g8 [(a8)] b8 [(c8)] | % 4
    a4 r4 a4 | % 5
    a8 (c8) c4. d8 | % 6
	<< { \voiceOne c8_(b8) b4 a8 a8 }
    \new Voice { \voiceTwo s4 b4. a8 } | % 7
	>> \oneVoice
    g4 g8 [(a8)] b8 [(c8)] | % 8
    a4 r4 a4 | % 9
    f'8 \(c8\) c4. f8 | \barNumberCheck #10
    g8 (c,8) c4. e8 | % 11
    d8 \< \(e8\) f4 g4 \! | % 12
    e4 \> c4 \! b4 | % 13
	<< { \voiceOne a4 c4 d8 d8 }
    \new Voice { \voiceTwo s4 c4. d8 } | % 14
	>> \oneVoice
    c8 (b8) b4. a8 | % 15
    g8 \(a8\) b4 c4 | % 16
    a4 r4 \bar "|."
}

PartPFourVoiceOne =  \relative f {
    \clef "bass" \key f \major \time 3/4
    \mergeDifferentlyDottedOn
    \phrasingSlurDashed
    \partial 4 f4 \p | % 1
    f8 \(f8\) f4. f8 | % 2
    f4 f4 f8 \(f8\) | % 3
    c4 c4 c4 | % 4
    f4 r4 f4 | % 5
    f4 f4. f8 | % 6
	<< { \voiceOne f4 f4 f8 f8 }
    \new Voice { \voiceTwo s4 f4. f8 } | % 7
	>> \oneVoice
    c4 c4 c4 | % 8
    f4 r4 f4 | % 9
    f8 \(f8\) f4. f8 | \barNumberCheck #10
    e4 e4. c8 | % 11
    g'8 \< \(g8\) g4 g,4 \! | % 12
    c4 \> e4 \! g4 | % 13
	<< { \voiceOne a8_(f8) f4 f8 f8 }
    \new Voice { \voiceTwo s4 f4. f8 } | % 14
	>> \oneVoice
    f8 (b,8) b4. b8 | % 15
    c8 \(c8\) c4 c4 | % 16
    f,4 r4 \bar "|."
}

% The score definition
\score {
	\new ChoirStaff {
		<<
			\new Staff
			{ \PartPOneVoiceOne }
			\addlyrics { \PartPOneVoiceOneLyricsOne }
			\addlyrics { \PartPOneVoiceOneLyricsTwo }

			\new Staff
			{ \PartPTwoVoiceOne }
			\addlyrics { \PartPOneVoiceOneLyricsOne }
			\addlyrics { \PartPOneVoiceOneLyricsTwo }

			\new Staff
			{ \PartPThreeVoiceOne }
			\addlyrics { \PartPOneVoiceOneLyricsOne }
			\addlyrics { \PartPOneVoiceOneLyricsTwo }

			\new Staff
			{ \PartPFourVoiceOne }
		>>
	}
    \layout {}
    \midi {}
}

