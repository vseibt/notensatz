\version "2.18.2"
\language deutsch

#(set-global-staff-size 18)

%\paper {
%	print-page-number = ##f
%%	% ragged-bottom = ##t
%	system-system-spacing #'basic-distance = #18
%	system-system-spacing #'minimum-distance = #16
%	annotate-spacing = ##f	% Darstellung änderbarer Abstände
%}

\header {
  title = "Nun schürz dich, Gretlein"
  composer = "Johnann Eccard 1553-1611"
  subtitle = "aus \"Neue Lieder ...\", 1589"
  copyright = "© Volker Seibt 2018 (Lizenz CC BY-SA 3.0 DE)"
  % Voreingestellte LilyPond-Tagline entfernen
  tagline = ##f
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
	\time 2/2
	\key c \major
}

barSpace = {
	\hide Staff.BarLine
	s1*15
	\undo \hide Staff.BarLine
	s1
	\hide Staff.BarLine
	s1*9
	\undo \hide Staff.BarLine
}

sopranoVoice = \relative c' {
	\global
	\dynamicUp
	d'2 d4 d |
	h h c2 |
	a4 a h h |
	g2. g4 |
	a2 g |
	e4 f e4. e8 |
	fis4 \breathe a a a |
	fis fis g2 |
	e4 g c c |
	h2 a |
	h1 |
	r4 g g g |
	a1. a2 |
	a1 |
	\repeat volta2 {
		r4 a h c |
    	d( c8 h a4) h |
    	c2 h |
    	\breathe h d4 d |
    	c4.( d8 e2) |
    	d h |
    	r4 d e1 d2 |
    	d2. d4 |
	} \alternative {
		{ d1 | }
		{ d \bar "|." }
	}
}

verseSopranoVoiceOne = \lyricmode {
  \set stanza = #"1. "
  Nun schürz dich Gret -- lein, schürz dich,
  du musst mit mir da -- von,
  du musst mit mir da -- von,
  nun schürz dich Gret -- lein, schürz dich,
  du musst mit mir da -- von,
  du musst mit mir __ da -- von;
  das Korn ist ab -- ge -- schnit -- ten,
  der Wein ist ein -- ge -- tan, der Wein __ ist ein -- ge -- tan, tan.
}

verseSopranoVoiceTwo = \lyricmode {
  \set stanza = #"2. "
  Sieh, Häns -- lein, lie -- bes Häns -- lein,
  so lass mich bei dir sein,
  so lass mich bei dir sein,
  sieh, Häns -- lein, lie -- bes Häns -- lein,
  so lass mich bei dir sein,
  so lass mich bei dir sein;
  die Wo -- chen auf __ dem Fel -- de,
  den Fei -- er -- tag __ beim Wein, den Fei -- er -- tag beim Wein, Wein.
}

sopranoVoicePart = \new Staff \with {
  instrumentName = "Sopran"
  midiInstrument = "choir aahs"
} { << \barSpace \transpose d c { \sopranoVoice } >> }
\addlyrics { \verseSopranoVoiceOne }
\addlyrics { \verseSopranoVoiceTwo }

altoVoice = \relative c' {
	\global
	\dynamicUp
	r2 g' |
	g4 g e e |
	fis2 g4 g |
	d2 e |
	d2. d4 |
	a'2 r4 a |
	a a fis fis |
	a2 d,4 d |
	g e2 a4.( g8 g2) fis4 |
	g \breathe g g g |
	d d e2 |
	c4 c f2 |
	e4 d2 cis4 |
	d2 r4 d |
	\repeat volta 2 {
		e f g( f8 e |
		d4) e f( e8 d |
		c2) d4 \breathe d |
		g2 a4 a2 a4 g2 \breathe |
		h d4 d |
		a4.( h8 c1) h2 a1( |
	} \alternative {
		{ h2) r4 d, | }
		{ h'1\repeatTie \bar "|." }
	}
}

verseAltoVoiceOne = \lyricmode {
  \set stanza = #"1. "
  Nun schürz dich Gret -- lein, schürz dich,
  du musst mit mir da -- von,
  nun schürz dich Gret -- lein, schürz dich,
  du musst mit mir __ da -- von,
  nun schürz dich Gret -- lein, schürz dich,
  du musst mit mir __ da -- von;
  das Korn ist ab -- ge -- schnit -- ten,
  der Wein ist ein -- ge -- tan, der Wein ist ein -- ge -- tan, __ das
}

verseAltoVoiceTwo = \lyricmode {
  \set stanza = #"2. "
  Sieh, Häns -- lein, lie -- bes Häns -- lein,
  so lass mich bei dir sein,
  sieh, Häns -- lein, lie -- bes Häns -- lein,
  so lass mich bei __ dir sein,
  sieh, Häns -- lein, lie -- bes Häns -- lein,
  so lass mich bei __ dir sein;
  die Wo -- chen auf __ dem Fel -- de,
  den Fei -- er -- tag beim Wein, den Fei -- er -- tag __ beim Wein, __ die
}

altoVoicePart = \new Staff \with {
  instrumentName = "Alt"
  midiInstrument = "choir aahs"
} { << \barSpace \transpose d c { \altoVoice } >> }
\addlyrics { \verseAltoVoiceOne }
\addlyrics { \verseAltoVoiceTwo }

tenorVoice = \relative c' {
	\global
	\dynamicUp
	r1 |
	r |
	d2 d4 d |
	h h c2 |
	a4 a h2 |
	cis4 d2 cis4 |
	d1 |
	r |
	r |
	r |
	d2 d4 d |
	h h c2 |
	a d |
	e4 f e4. e8 |
	fis1 |
	\repeat volta 2 {
		r |
		r2 d |
		e4 f g( f8 e |
		d4) e f2 |
		e r4 e |
		g g d g |
		fis \breathe fis a2 |
		g g1 fis2 |
	} \alternative {
		{ g1 | }
		{ g }
	}
}

verseTenorVoiceOne = \lyricmode {
  \set stanza = #"1. "
  Nun schürz dich Gret -- lein, schürz dich,
  du musst mit mir da -- von,
  nun schürz dich Gret -- lein, schürz dich,
  du musst mit mir da -- von;
  das Korn ist ab -- ge -- schnit -- ten,
  der Wein ist ein -- ge -- tan, der Wein ist ein -- ge -- tan, tan.
}

verseTenorVoiceTwo = \lyricmode {
  \set stanza = #"2. "
  Sieh, Häns -- lein, lie -- bes Häns -- lein,
  so lass mich bei dir sein,
  sieh, Häns -- lein, lie -- bes Häns -- lein,
  so lass mich bei dir sein;
  die Wo -- chen auf __ dem Fel -- de,
  den Fei -- er -- tag beim Wein, den Fei -- er -- tag __ beim Wein, Wein.
}

tenorVoicePart = \new Staff \with {
  instrumentName = "Tenor"
  midiInstrument = "choir aahs"
} { \clef "treble_8" << \barSpace \transpose d c { \tenorVoice } >> }
\addlyrics { \verseTenorVoiceOne }
\addlyrics { \verseTenorVoiceTwo }

bassVoice = \relative c {
	\global
	\dynamicUp
	r1 |
	r |
	r2 g' |
	g4 g e e |
	fis2 g4 g |
	a d, a'4. a8 |
	d,2 r4 d' |
	d d h h |
	c2 a4 f |
	g h c d |
	g,2 \breathe g |
	g4 g e e |
	f2 d4 d' |
	cis d a4. a8 |
	d,1 |
	\repeat volta 2 {
		r2 r4 a' |
		h c d( c8 h |
		a4) a g2.( f8 e d2) |
		a'4 \breathe a c c |
		g2. g4 |
		d'2 r4 a |
		c c g8( a h c |
		d2.) d4 |
	} \alternative {
		{ g,1 }
		{ g }
	}
}

verseBassVoiceOne = \lyricmode {
  \set stanza = #"1. "
  Nun schürz dich Gret -- lein, schürz dich,
  du musst mit mir da -- von,
  nun schürz dich Gret -- lein, schürz dich,
  du musst mit mir da -- von,
  nun schürz dich Gret -- lein, schürz dich,
  du musst mit mir da -- von;
  das Korn ist ab -- ge -- schnit -- ten,
  der Wein ist ein -- ge -- tan, der Wein ist ein -- ge -- tan, tan.
}

verseBassVoiceTwo = \lyricmode {
  \set stanza = #"2. "
  Sieh, Häns -- lein, lie -- bes Häns -- lein,
  so lass mich bei dir sein,
  sieh, Häns -- lein, lie -- bes Häns -- lein,
  so lass mich bei dir sein,
  sieh, Häns -- lein, lie -- bes Häns -- lein,
  so lass mich bei dir sein;
  die Wo -- chen auf __ dem Fel -- de,
  den Fei -- er -- tag __ beim Wein, den Fei -- er -- tag __ beim Wein, Wein.
}

bassVoicePart = \new Staff \with {
  instrumentName = "Bass"
  midiInstrument = "choir aahs"
} { \clef bass << \barSpace \transpose d c { \bassVoice } >> }
\addlyrics { \verseBassVoiceOne }
\addlyrics { \verseBassVoiceTwo }

%\book {
%	\bookOutputSuffix "nun-schuerz-dich"
	\score {
		\new StaffGroup {
		  <<
		   \sopranoVoicePart
		   \altoVoicePart
		   \tenorVoicePart
		   \bassVoicePart
		  >>
		}
		\layout {}
	}

	\score {
		\new StaffGroup {
		\unfoldRepeats
		  <<
		   \sopranoVoicePart
		   \altoVoicePart
		   \tenorVoicePart
		   \bassVoicePart
		  >>
		}
		\midi { \tempo 4=100 }
	}
%}
