
\include "header.ly"

\include "sopranoVoice.ly"
\include "altoVoice.ly"
\include "tenorVoice.ly"
\include "bassVoice.ly"

#(set-global-staff-size 22)

\paper {
	print-page-number = ##f
%	% ragged-bottom = ##t
%	system-system-spacing #'basic-distance = #8
}

\header {
  title = "Come away, come sweet love"
  composer = "John Dowland 1562-1626"
  subtitle = "\"The first Booke of Songs or Ayres\", 1597"
  copyright = "© Volker Seibt 2018 (Lizenz CC BY-SA 3.0 DE)"
  % Voreingestellte LilyPond-Tagline entfernen
  tagline = ##f
}

\score {
  <<
   \sopranoVoicePart
   \altoVoicePart
   \tenorVoicePart
   \bassVoicePart
  >>

  \layout { }
  \midi {
    \tempo 4=100
  }
}

% mit dem folgenden Befehl werden änderbare Abstände dargestellt
% \paper { annotate-spacing = ##t }
