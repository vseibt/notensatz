
\include "header.ly"

#(set-default-paper-size "a4landscape")

\include "sopranoVoice.ly"
\include "altoVoice.ly"
\include "tenorVoice.ly"
\include "bassVoice.ly"

% #(set-global-staff-size 25)

\paper {
	print-page-number = ##f
}

\bookpart {
	\header {
	  title = "Come away, come sweet love"
	  composer = "John Dowland 1562-1626"
	  subtitle = "\"The first Booke of Songs or Ayres\", 1597"
%	  copyright = "© Volker Seibt 2018 (Lizenz CC BY-SA 3.0 DE)"
	  % Voreingestellte LilyPond-Tagline entfernen
	  tagline = ##f
	}
	\score {
	  <<
	   \sopranoVoicePart
	  >>

	  \layout { }
	}
}

\bookpart {
	\header {
	  copyright = "© Volker Seibt 2018 (Lizenz CC BY-SA 3.0 DE)"
	  % Voreingestellte LilyPond-Tagline entfernen
	  tagline = ##f
	}
	\score {
	  <<
	   \altoVoicePart
	  >>

	  \layout { }
	}
}

\bookpart {
	\score {
	  <<
	   \tenorVoicePart
	  >>

	  \layout { }
	}
}

\bookpart {
	\header {
	  copyright = "© Volker Seibt 2018 (Lizenz CC BY-SA 3.0 DE)"
	  % Voreingestellte LilyPond-Tagline entfernen
	  tagline = ##f
	}
	\score {
	  <<
	   \bassVoicePart
	  >>

	  \layout { }
	}
}

% mit dem folgenden Befehl werden änderbare Abstände dargestellt
% \paper { annotate-spacing = ##t }
