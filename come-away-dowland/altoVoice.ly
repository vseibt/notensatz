
\include "header.ly"

altoVoice = \relative c'' {
  \global
  \dynamicUp
  g4 g a2 a4 g fis2 (|
  fis4) g a a a4. g8 fis2 |
  g4 g a2 a4 g fis2 (|
  fis4) g a a a4. g8 fis2 |
  b8 c d4. (c8) b4 a g fis2 |
  \time 3/2 g2 g4 f4. g16 (a b4) |
  b8 a16 (g a8 b4 a8) b2 f4 |
  f2 d4 d2 d4 | g2 fis4 g2. |
  \time 4/2 b8 c d4. (c8) b4 a g fis2 |
  \time 3/2 g2 g4 f4. g16 (a b4) |
  b8 a16 (g a8 b4 a8) b2 f4 |
  f2 d4 d2 d4 | g2 fis4 g2. \bar "|."
}

verseAltoVoiceOne = \lyricmode {
  \set stanza = #"1. "
  Come a -- way, come sweet love, __
  The gol -- den mor -- ning breaks.
  All the earth, all the air __
  of love and plea -- sure speaks:
  Teach thine arms __ then to em -- brace,
  And sweet ro -- sy __ lips to __ kiss,
  And mix our souls in mu -- tual bliss,
  Eyes were made __ for beau -- ty's grace,
  View -- ing, rue -- ing __ love's long __ pain
  Pro -- cur'd by beau -- ty's rude dis -- dain.
}

verseAltoVoiceTwo = \lyricmode {
  \set stanza = #"2. "
  Come a -- way, come sweet love, __
  The gol -- den mor -- ning wastes,
  While the sun from his sphere __
  his fie -- ry ar -- rows casts,
  Ma -- king all __ the sha -- dows fly,
  Play -- ing, Stay -- ing __ in the __ grove
  To en -- ter -- tain the stealth of love.
  Thi -- ther, sweet __ love, let us hie,
  Fly -- ing, dy -- ing __ in de -- sire
  Wing'd with sweet hopes and heav'n -- ly fire.
}

verseAltoVoiceThree = \lyricmode {
  \set stanza = #"3. "
  Come a -- way, come sweet love, __
  Do not in vain a -- dorn
  Beau -- ty's grace, that should rise __
  like to the na -- ked morn.
  Li -- lies on __ the ri -- ver -- side
  And the fair2 Cyprian flow'rs new -- blown
  De -- sire no beau -- ties but their own,
  Or -- na -- ment __ is nurse of pride,
  Plea -- sure, mea -- sure __ love's de -- light.
  Haste then, sweet love, our wi -- shed flight!
}

altoVoicePart = \new Staff \with {
  instrumentName = "Alt"
  midiInstrument = "choir aahs"
} { \altoVoice }
\addlyrics { \verseAltoVoiceOne }
\addlyrics { \verseAltoVoiceTwo }
\addlyrics { \verseAltoVoiceThree }
