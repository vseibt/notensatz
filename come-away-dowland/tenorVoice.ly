
\include "header.ly"

tenorVoice = \relative c' {
  \global
  \dynamicUp
  h4 c c2 f4 d d2 |
  d2 d4 f e4. e8 d2 |
  h4 c c2 f4 d d2 |
  d2 d4 f e4. e8 d2 |
  f4 f b, f' f d d2 |
  \time 3/2 b4. (c16 d) es4 c f2 |
  d4 f2 f b,4 | a4. (d8) c (b) a2 b8 (c |
  d4) d4. c8 h2. |
  \time 4/2 f'4 f b, f' f d d2 |
  \time 3/2 b4. (c16 d) es4 c f2 |
  d4 f2 f b,4 | a4. (d8) c (b) a2 b8 (c |
  d4) d4. c8 h2. \bar "|."
}

verseTenorVoiceOne = \lyricmode {
  \set stanza = #"1. "
  Come a -- way, come sweet love,
  The gol -- den mor -- ning breaks.
  All the earth, all the air
  of love and plea -- sure speaks:
  Teach thine arms then to em -- brace,
  And __ sweet ro -- sy __ lips to kiss,
  And mix __ our __ souls in __ mu -- tual bliss,
  Eyes were made for beau -- ty's grace,
  View -- ing, rue -- ing love's long pain
  Pro -- cur'd __ by __ beau -- ty's __ rude dis -- dain.
}

verseTenorVoiceTwo = \lyricmode {
  \set stanza = #"2. "
  Come a -- way, come sweet love,
  The gol -- den mor -- ning wastes,
  While the sun from his sphere
  his fie -- ry ar -- rows casts,
  Ma -- king all the sha -- dows fly,
  Play -- ing, Stay -- ing __ in the grove
  To en -- ter -- tain the __ stealth of love.
  Thi -- ther, sweet love, let us hie,
  Fly -- ing, dy -- ing in de -- sire
  Wing'd with __ sweet __ hopes and __ heav'n -- ly fire.
}

verseTenorVoiceThree = \lyricmode {
  \set stanza = #"3. "
  Come a -- way, come sweet love,
  Do not in vain a -- dorn
  Beau -- ty's grace, that should rise
  like to the na -- ked morn.
  Li -- lies on the ri -- ver -- side
  \set ignoreMelismata = ##t And the _ \unset ignoreMelismata fair Cyprian flow'rs new -- blown
  De -- sire no __ beau -- ties but __ their __ _ own,
  Or -- na -- ment is nurse of pride,
  Plea -- sure, mea -- sure love's de -- light.
  Haste then, __ sweet __ love, our __ wi -- shed flight!
}

tenorVoicePart = \new Staff \with {
  instrumentName = "Tenor"
  midiInstrument = "choir aahs"
} { \clef "treble_8" \tenorVoice }
\addlyrics { \verseTenorVoiceOne }
\addlyrics { \verseTenorVoiceTwo }
\addlyrics { \verseTenorVoiceThree }
