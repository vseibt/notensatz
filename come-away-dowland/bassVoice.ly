
\include "header.ly"

bassVoice = \relative c' {
  \global
  \dynamicUp
  g4 e f2 f4 g d2 |
  d4 g (f) e8 (d) a'4 a, d2 |
  g4 e f2 f4 g d2 |
  d4 g f e8 (d) a'4 a, d2 |
  b4. c8 d4. e8 f4 g d2 |
  \time 3/2 es4. d8 (c4) f4. es8 (d4) |
  g4 f2 b, b4 |
  f'2 g4 d4. c8 (b a | g4) d' d g,2. |
  \time 4/2 b4. c8 d4. e8 f4 g d2 |
  \time 3/2 es4. d8 (c4) f4. es8 (d4) |
  g4 f2 b, b4 |
  f'2 g4 d4. c8 (b a | g4) d' d g,2. \bar "|."
}

verseBassVoiceOne = \lyricmode {
  \set stanza = #"1. "
  Come a -- way, come sweet love,
  The gol -- den __ mor -- ning breaks.
  All the earth, all the air
  of lo -- ve and __ plea -- sure speaks:
  Teach thine arms then to em -- brace,
  And sweet ro -- sy __ lips to kiss,
  And mix our souls in __ mu -- tual bliss,
  Eyes were made for beau -- ty's grace,
  View -- ing, rue -- ing love's long pain
  Pro -- cur'd by beau -- ty's __ rude dis -- dain.
}

verseBassVoiceTwo = \lyricmode {
  \set stanza = #"2. "
  Come a -- way, come sweet love,
  The gol -- den __ mor -- ning wastes,
  While the sun from his sphere
  his fie -- _ ry __ ar -- rows casts,
  Ma -- king all the sha -- dows fly,
  Play -- ing, Stay -- ing __ in the grove
  To en -- ter -- tain the __ stealth of love.
  Thi -- ther, sweet love, let us hie,
  Fly -- ing, dy -- ing __ in de -- sire
  Wing'd with sweet \set ignoreMelismata = ##t ho -- _ pes __ _ and \unset ignoreMelismata heav'n -- ly fire.
}

verseBassVoiceThree = \lyricmode {
  \set stanza = #"3. "
  Come a -- way, come sweet love,
  Do not __ in __ vain a -- dorn
  Beau -- ty's grace, that should rise
  li -- ke to the __ na -- ked morn.
  Li -- lies on the ri -- ver -- side
  And the fair Cyprian flow'rs new -- blown
  De -- sire no beau -- ties __ but their own,
  Or -- na -- ment is nurse of pride,
  Plea -- sure, mea -- sure __ love's de -- light.
  Haste then, sweet love, our __ wi -- shed flight!
}

bassVoicePart = \new Staff \with {
  instrumentName = "Bass"
  midiInstrument = "choir aahs"
} { \clef bass \bassVoice }
\addlyrics { \verseBassVoiceOne }
\addlyrics { \verseBassVoiceTwo }
\addlyrics { \verseBassVoiceThree }
