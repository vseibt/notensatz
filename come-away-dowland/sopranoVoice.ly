
\include "header.ly"

sopranoVoice = \relative c'' {
  \global
  \dynamicUp
  d4 c c2 c4 b a2 | a4 b c d2 cis4 d2 |
  d4 c c2 c4 b a2 | a4 b c d2 cis4 d2 |
  d4. e8 f4 d c b a2 |
  \time 3/2 g4. a16 (b c4) a4. b16 (c d4 |
  b) c c d2 d4 | c4. b8 (a g) fis2 g4 |
  b a2 g2. |
  \time 4/2 d'4. e8 f4 d c b a2 |
  \time 3/2 g4. a16 (b c4) a4. b16 (c d4 |
  b) c c d2 d4 | c4. b8 (a g) fis2 g4 |
  b a2 g2. \bar "|."
}

verseSopranoVoiceOne = \lyricmode {
  \set stanza = #"1. "
  Come a -- way, come sweet love,
  The gol -- den mor -- ning breaks.
  All the earth, all the air
  of love and plea -- sure speaks:
  Teach thine arms then to em -- brace,
  And sweet ro -- sy __ lips to kiss,
  And mix our __ souls in mu -- tual bliss,
  Eyes were made for beau -- ty's grace,
  View -- ing, __ rue -- ing __ love's long pain
  Pro -- cur'd by __ beau -- ty's rude dis -- dain.
}

verseSopranoVoiceTwo = \lyricmode {
  \set stanza = #"2. "
  Come a -- way, come sweet love,
  The gol -- den mor -- ning wastes,
  While the sun from his sphere
  his fie -- ry ar -- rows casts,
  Ma -- king all the sha -- dows fly,
  Play -- ing, __ Stay -- ing __ in the grove
  To en -- ter -- tain the stealth of love.
  Thi -- ther, sweet love, let us hie,
  Fly -- ing, __ dy -- ing __ in de -- sire
  Wing'd with sweet __ ho -- pes and heav'nly fire.
}

verseSopranoVoiceThree = \lyricmode {
  \set stanza = #"3. "
  Come a -- way, come sweet love,
  Do not in vain a -- dorn
  Beau -- ty's grace, that should rise
  like to the na -- ked morn.
  Li -- lies on the ri -- ver -- side
  And the fair Cyprian flow'rs new -- blown
  \set ignoreMelismata = ##tDe -- sire __ _ no _ \unset ignoreMelismata beau -- ties but their own,
  Or -- na -- ment is nurse of pride,
  Plea -- sure, mea -- sure __ love's de -- light.
  Haste then, sweet __ love, our wi -- shed flight!
}

sopranoVoicePart = \new Staff \with {
  instrumentName = "Sopran"
  midiInstrument = "choir aahs"
} { \sopranoVoice }
\addlyrics { \verseSopranoVoiceOne }
\addlyrics { \verseSopranoVoiceTwo }
\addlyrics { \verseSopranoVoiceThree }

