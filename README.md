# Notensatz

Mit Lilypond gesetzte Noten inklusive der Definitionsdateien.

### Dateien:

- [Come away, John Dowland](publish/come-away-dowland.pdf)
- [Nun Schürz dich, Johann Eccard](publish/nun-schuerz-dich.pdf) (Quelle: [https://jbc.bj.uj.edu.pl/dlibra/publication/292093/edition/279412](https://jbc.bj.uj.edu.pl/dlibra/publication/292093/edition/279412))
