\version "2.18.2"
\language "deutsch"
date = #(strftime "%Y-%m-%d" (localtime (current-time)))
%\include "../date.ly" % date = #(strftime "%-d. %-m. %Y" (localtime (current-time)))

\header {
  title = "Die Lore-Ley"
  composer = "Friedrich Silcher (1789 - 1860)"
  poet = "Heinrich Heine (1797 - 1856)"
  copyright = \markup { "Copyright © by CPDL /" \date  "/ J. B." }
	% Voreingestellte LilyPond-Tagline entfernen
	tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \tempo "Andante" 8 = 120
  \key es \major
  \time 6/8
  \partial 8
  \autoBeamOff
}
#(set-global-staff-size 21)
globalTempo = {
}

soprano =  {
  \global
  b' 8  |
  \slurDashed
  b' 8.([ c'' 16]) b' 8 es''([ d'']) c''  |
  b' 4. as' 4 as' 8  |
  g' 8([ g']) g' \slurSolid f'([ es']) f'  |
  %% 5
  g' 4.~ g' 8 r b'  |
  b' 8. c'' 16 b' 8 es''([ d'']) c''  |
  \slurDashed
  b' 4. as' 4 as' 8  |
  g'8([ g']) g' b' as' f'  |
  es' 4.~ es'8 r8 g'  |
  %% 10
  f' 8.([ g'16]) f' 8 b' f' f'  |
  d'' 4. c'' 4 c'' 8  |
  b' 4 b' 8 a'([ b']) c''  |
  b' 4.~b'4 b'8  |
  \slurSolid
  b' 8. c'' 16 b' 8 es''([ d'']) c''  |
  %% 15
  b' 4( g''8) f'' 4 f'' 8  |
  \slurDashed
  es''8.([ es''16]) es''8 \slurSolid d''([ c'']) d''  |
  es'' 4.~es''8  r8
  \bar "|."

}

alto = {
  \global
  es'8\p  |
  \slurDashed
  es'8.([ es'16]) es' 8 es'([ es']) es' 8  |
  es' 4. f' 4 f' 8  |
  es'8([ es']) es' \slurSolid d'([ c']) d'  |
  %% 5
  es' 4.~es'8 r8 es'  |
  es' 8. es' 16 es' 8 es' 4 es' 8  |
  es' 4. f' 4 f' 8  |
  \slurDashed
  es'([ es']) es' 8 d' d' d'  |
  b4.~b8 r8 es'  |
  %% 10
  d' 8.([ es'16]) d' 8 d' d' f'  |
  f' 4. g' 4 g' 8  |
  \slurDashed
  f' 4 f' 8 f'8([ f']) f'8  |
  \slurSolid
  f'4( g' 8 as'[ g']) f'  |
  g' 8. as' 16 g' 8 c''([ b']) as'  |
  %% 15
  g'4( b'8) as' 4 as' 8  |
  \slurDashed
  g'8.([ g'16]) g' 8 f' 4 as' 8  |
  g' 4.~g'8 r8
  \bar "|."

}

tenor =  {
  \global
  \slurDashed
  g 8  |
  g 8.([ as16]) g 8 c'([ b]) as  |
  g 4. c' 4 c' 8  |
  b([ b]) b 8 b 4 b 8  |
  %% 5
  b 4.~b8 r8 g  |
  \slurSolid
  g 8. as 16 g 8 c'8([ b]) as  |
  g 4. c' 4 c' 8  |
  \slurDashed
  b([ b]) g 8 f f as  |
  g4.~g8 r8 b  |
  %% 10
  b8.([ b16]) b 8 b b b  |
  \slurSolid
  b 4. g8([ c']) es'  |
  \slurDashed
  d' 4 d' 8 c'([ d']) es'  |
  \slurSolid
  d'4( es' 8 f'[ es']) d'  |
  es' 8. es' 16 es' 8 es' 4 es' 8  |
  %% 15
  es' 4. c' 4 c' 8  |
  \slurDashed
  b8.([ b16]) b8 b 4 b 8  |
  b 4.~b8 r8
  \bar "|."

}

bass = {
  \global
  \slurDashed
  es 8  |
  es8.([ es16]) es 8 es([ es]) es 8  |
  es 4. as, 4 as, 8  |
  b,8([ b,]) b, b, 4 b, 8  |
  %% 5
  es 4.~es8 r8 es  |
  es 8. es 16 es 8 es 4 es 8  |
  es 4. as, 4 as, 8  |
  \slurDashed
  b,8([ b,]) b, b, b, b,  |
  es4.~es8 r8 es  |
  %% 10
  b,8.([ b,16]) b, 8 b, f d  |
  b, 4. es 4 es 8  |
  f 4 f 8 f8([ f]) f8  |
  b,4.~ b, 4 b, 8  |
  es 8. es 16 es 8 es 4 es 8  |
  %% 15
  es 4. as 4 as, 8  |
  \slurDashed
  b,8.([ b,16]) b, 8 b, 4 b, 8  |
  es 4.~es8 r8
  \bar "|."

}

verseOne = \lyricmode {
  \set stanza = "1."
  \set ignoreMelismata = ##t
  Ich weiß nicht was soll es be -- deu -- ten,
  \set ignoreMelismata = ##f
  dass ich __ so trau -- rig bin; __
  ein Mär -- chen aus al -- ten Zei -- ten,
  das kommt mir nicht aus dem Sinn. __

  Die Luft ist kühl und es dun -- kelt,
  und ru -- hig fließt der Rhein; __
  der Gip -- fel des Ber -- ges fun -- kelt
  im A -- bend -- son -- nen -- schein. __
}

verseTwo = \lyricmode {
  \set stanza = "2."

  Die schön -- ste Jung -- frau sit -- zet
  dort o -- ben wun -- der -- bar; __
  ihr gold -- nes Ge -- schmei -- de blit -- zet,
  sie kämmt ihr gol -- de -- nes Haar. __
  \set ignoreMelismata = ##t
  Sie kämmt es mit gol -- de -- nem Kam -- me
  \set ignoreMelismata = ##f
  und singt ein Lied __ da -- bei; __
  das hat ei -- ne wun -- der -- sa -- me,
  \set ignoreMelismata = ##t
  ge -- wal -- ti -- ge  \set ignoreMelismata = ##f Me -- lo -- dei. __
}

verseThree = \lyricmode {
  \set stanza = "3."
  \set ignoreMelismata = ##t
  Den Schif -- fer im \set ignoreMelismata = ##f klei -- nen Schif -- fe
  \set ignoreMelismata = ##t
  er -- greift es mit \set ignoreMelismata = ##f  wil -- dem Weh; __
  er schaut nicht die Fel -- sen -- rif -- fe,
  \set ignoreMelismata = ##t
  er schaut nur hin -- auf in die \set ignoreMelismata = ##f Höh. __
  \set ignoreMelismata = ##t
  Ich glau -- be, die Wel -- len ver -- schlin -- gen
  am En -- de Schif -- fer und \set ignoreMelismata = ##f Kahn; __
  und das hat mit ih -- rem Sin -- gen
  die Lo -- re -- ley __ ge -- tan. __
}

choir = \new ChoirStaff <<
  \new Staff \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "Sopran" "Alt" }
  } <<
    \new Voice = "soprano" \with {
      \consists "Ambitus_engraver"
    } { \voiceOne \soprano }
    \new Voice = "alto" \with {
      \consists "Ambitus_engraver"
      \override Ambitus #'X-offset = #2.0
    } { \voiceTwo \alto }
  >>
  \new Lyrics \with {
    \override VerticalAxisGroup #'staff-affinity = #CENTER
  } \lyricsto "soprano" \verseOne
  \new Lyrics \with {
    \override VerticalAxisGroup #'staff-affinity = #CENTER
  } \lyricsto "soprano" \verseTwo
  \new Lyrics \with {
    \override VerticalAxisGroup #'staff-affinity = #CENTER
  } \lyricsto "soprano" \verseThree
  \new Staff \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "Tenor" "Bass" }
  } <<
    \clef bass
    \new Voice = "tenor" \with {
      \consists "Ambitus_engraver"
    } { \voiceOne \tenor }
    \new Voice = "bass" \with {
      \consists "Ambitus_engraver"
      \override Ambitus #'X-offset = #2.0
    } { \voiceTwo \bass }
  >>
>>


\score {
  \choir
  \layout { }
  \midi {
    \context {
      \Score
      tempoWholesPerMinute = #(ly:make-moment 120 8)
    }
  }
}
