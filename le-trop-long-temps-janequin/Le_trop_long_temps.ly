
\version "2.18.2"
% automatically converted by musicxml2ly from Le_trop_long_temps.xml

%% additional definitions required by the score:
\language "deutsch"

#(set-global-staff-size 18)

\paper {
	#(set-paper-size "a4")
%	system-system-spacing = #'((padding . 0) (basic-distance . 1))
	ragged-last-bottom = ##f
	ragged-bottom = ##f
%	print-all-headers = ##t
%	#(define page-breaking ly:page-turn-breaking)
%	systems-per-page = 3
%	system-count = 7
%	page-count = 2
%	indent = 0
%	ragged-bottom = ##t
%	system-system-spacing #'basic-distance = #10
%	top-markup-spacing #'basic-distance = #10
%	top-system-spacing #'basic-distance = #10
%	system-system-spacing #'minimum-distance = #10
%	annotate-spacing = ##t	% Darstellung änderbarer Abstände
}

\header {
    encodingsoftware = "MuseScore 2.0.2"
    encodingdate = "2018-11-18"
%    composer = "Clement Janequin, (ca. 1485-1558)"
    title = "1. Le trop long temps"
    subsubtitle = "aus \"42 Chansons musicales a troys parties\" (Pierre Attaingnant, 1529)"
	copyright = "© Volker Seibt 2018 (Lizenz CC BY-SA 3.0 DE)"
	% Voreingestellte LilyPond-Tagline entfernen
	tagline = ##f
}

\layout {
    \context {
    	\Score
        	skipBars = ##t
	}
    \context {
	    \Staff
		    \consists "Ambitus_engraver"
    }
}

Superius =  \relative g' {
    \clef "treble" \key c \major \time 2/2 \partial 2 r4 g4 | % 1
    c2 h4 a2
    g8 f8 e2 | % 3
    r4 g4 a4 g4 | % 4
    c4. h8 a4 g4.
    f8 e8 d8 c8 d8 e8 f8 | % 6
    g8 a8 h8 g8 a8 g8 c2
    h4 c2 | % 8
    r4 g4 a2 | % 9
    h2 c2.
    h8 a8 h2 | % 11
    r4 g4 c4. h8 | % 12
    a4 a4 g4 f4.
    e8 e4. d16 c16 d4 | % 14
    e2 r4 e4 | % 15
    e8 f8 g8 a8 h4 c2
    h8 a8 g2 | % 17
    r4 g4 g4 g4 | % 18
    f4 g4 c,8 d8 e8 f8 | % 19
    g4 a4. g8 g2
	\once \set suggestAccidentals = ##t
    fis4 g2 | % 21
	\repeat volta 2 {
	    r4 g4 c2 | % 22
	    h4 a2 g8 f8 | % 23
	    e2 r4 g4 | % 24
	    a4 g4 c4. h8 | % 25
	    a4 g4. f8 e8 d8 | % 26
	    c8 d8 e8 f8 g8 a8 h8 g8 | % 27
	    a8 g8 c2 h4 | % 28
	    c1
    }
}

SuperiusLyrics = \lyricmode {
	Le trop long temps __ _ _ _
	qu'ay es -- té sans __ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ te veoir
	A le mien cueur __ _ _ _
	bles -- sé de tel -- le sor -- _ _ _ _ _ _ te,
	Qu'eus -- se __ _ _ _ _ vou -- _ _ lu
	mil -- le fois es -- tre mor -- _ _ _ _ _ _ _ _ te
	si bon es -- poir __ _ _ _
	n'y eust vou -- lu __ _ _ pour -- _ _ _ _ _ _ _ _ _ _ _ _ _ veoir __ _ _
}

Tenor =  \relative e' {
    \clef "treble_8" \key c \major \time 2/2 \partial 2 e2 | % 1
    e2 d2 | % 2
    c1.
    r4 c4 | % 4
    c4 c4 d4 e4.
    d8 c8 h8 a8 h8 c8 d8 | % 6
    e8 f8 g8 e8 f8 e8 d8 c8 | % 7
    d2 c2 | % 8
    r1 | % 9
    d2 e2 | \barNumberCheck #10
    f2 g1
    r4 c,4 | % 12
    f4 f4 e4 d4.
    c8 c2 h4 | % 14
    c1 | % 15
    r4 c4 h4 a4 | % 16
    a8 h8 c8 d8 e4 d8 c8 | % 17
    h4 e4 e4 e4 | % 18
    d4 e2 d8 c8 | % 19
    h4 a4 c8 h8 a8 g8 | \barNumberCheck #20
    a2 e'2 | % 21
	\repeat volta 2 {
	    r4 e4 e2 | % 22
	    d2 c1. % 23
	    | % 24
	    r4 c4 c4 c4 | % 25
	    d4 e4. d8 c8 h8 | % 26
	    a8 h8 c8 d8 e8 f8 g8 e8 | % 27
	    f8 e8 d8 c8 d2 | % 28
	    c1
    }
}

TenorLyrics = \lyricmode {
	Le trop long temps
	qu'ay es -- té sans te __ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ veoir
	A le mien cueur
	bles -- sé de tel -- le sor -- _ te, __ _
	Qu'eus -- se vou -- lu __ _ _ _ _ _ _ _
	mil -- le fois es -- tre __ _ _ mor -- _ _ _ _ _ _ te
	si bon es -- poir
	n'y eust vou -- lu pour -- _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ veoir __ _
}

Bassus =  \relative c {
    \clef "bass" \key c \major \time 2/2 \partial 2 c2 | % 1
    c2 g'4 f2
    e8 d8 c2 | % 3
    r4 c4 f4 e4 | % 4
    a4. g8 f4 e4 | % 5
    c2 r2 | % 6
    r4 g'4 f4 a4 | % 7
    g4 g4 c,4 c'4 | % 8
    h4 c2 h8 a8 | % 9
    g4 h4 a1
    r4 g4 | % 11
    c4. h8 a4. g8 | % 12
    f4. e8 c4 d4 | % 13
	\once \set suggestAccidentals = ##t
    b4 c4 g'2 | % 14
    c,2 r4 c4 | % 15
    c8 d8 e8 f8 g4 a4 | % 16
    f2 e2 | % 17
    r4 c4 c4 c4 | % 18
    d4 c4 c4. d8 | % 19
    e4 f4. g8 e4 | \barNumberCheck #20
    d2 c2 | % 21
	\repeat volta 2 {
	    r4 c4 c2 | % 22
	    g'4 f2 e8 d8 | % 23
	    c2 r4 c4 | % 24
	    f4 e4 a4. g8 | % 25
	    f4 e4 c2 | % 26
	    r2 r4 g'4 | % 27
	    f4 a4 g4 g4 | % 28
	    c,1
    }
}

BassusLyrics = \lyricmode {
	Le trop long temps __ _ _ _
	qu'ay es -- té sans te veoir __ _ _
	qu'ay es -- té sans te veoir
	A le mien cueur __ _
	bles -- _ sé de tel -- le sor -- _ _ _ _ _ _ _ _ te,
	Qu'eus -- se __ _ _ _ _ vou -- lu __ _
	mil -- le fois es -- tre mor -- _ _ _ _ _ _ te
	si bon es -- poir __ _ _ _
	n'y eust vou -- lu pour -- veoir __ _ _
	n'y eust vou -- lu pour -- veoir __
}

hideBars = {
  \hide Staff.BarLine
  s2 s1*20
  % the final bar line is not interrupted
  \undo \hide Staff.BarLine
  s1
  \hide Staff.BarLine
  s1 * 7
  \undo \hide Staff.BarLine
}

% The score definition
\score {
    <<
        \new StaffGroup <<
            \new Staff <<
                \set Staff.instrumentName = "Superius"
                \context Staff <<
                    \context Voice = "Superius" { << \hideBars \Superius >> }
                    \new Lyrics \lyricsto "Superius" \SuperiusLyrics
                    >>
                >>
            \new Staff <<
                \set Staff.instrumentName = "Tenor"
                \context Staff <<
                    \context Voice = "Tenor" { << \hideBars \Tenor >> }
                    \new Lyrics \lyricsto "Tenor" \TenorLyrics
                    >>
                >>
            \new Staff <<
                \set Staff.instrumentName = "Bassus"
                \context Staff <<
                    \context Voice = "Bassus" { << \hideBars \Bassus >> }
                    \new Lyrics \lyricsto "Bassus" \BassusLyrics
                    >>
                >>

            >>

        >>
    \layout {}
    \midi {}
}
