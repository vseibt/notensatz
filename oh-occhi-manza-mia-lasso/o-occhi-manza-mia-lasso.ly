\version "2.18.2"
\language "deutsch"

#(set-global-staff-size 17.2)

\paper {
	#(set-paper-size "a4")
%	indent = 0
}
\header {
	title = "O occhi manza mia"
	composer = "Orlando di Lasso"
	copyright = "© Volker Seibt 2018 (Lizenz CC BY-SA 3.0 DE)"
	% Voreingestellte LilyPond-Tagline entfernen
	tagline = ##f
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
}

sopranoVoice = \relative c'' {
  \global
  \dynamicUp
  % Die Noten folgen hier.
  \repeat volta 2 {a2 f4 f g a b2 a r4 a g fis g a b2. a4 g f2 e4 f1}
  a2 h4 c a2 c h c r4 a a a b b a2 f4 a a c4( ~ c h) c2
  \repeat volta 2 {a4 a8 a fis4 fis g g8 g e4 e f f8 f g4 f e2 r4 f d2 r4 f d2 g f f4 f( ~ f e8 d e2) f1 \fermata}
}

verseOne = \lyricmode {
  % Liedtext folgt hier.
	\set stanza = "1."
	O oc -- chi man -- za mi -- a,
	o oc -- chi man -- za mia ci -- gli do -- ra -- ti.
	O fac -- cia d'u -- na lu -- na, o fac -- cia d'u -- na lu -- na stra -- lu -- cen -- ti.
}
verseTwo = \lyricmode {
  % Liedtext folgt hier.
	\set stanza = "2."
	O boc -- ca come zuc -- ca -- ro, o boc -- ca come zuc -- ca -- ro,
	im -- pa -- na -- to.
	O can -- ne che spec -- chia -- re, o can -- ne che spec -- chia -- re,
	fai la gen -- te.
	Tie -- ne -- mi men -- te gio -- ia mia bel -- la guar -- dam' un poc' a me,
	a me, a me, fa mi con -- tien -- to!
}
verseTwoAlto = \lyricmode {
  % Liedtext folgt hier.
	\set stanza = "2."
	O boc -- ca come zuc -- ca -- ro, o boc -- ca come zuc -- ca -- ro,
	im -- pa -- na -- to.
	O can -- ne che spec -- chia -- re, o can -- ne che spec -- chia -- re,
	fai la gen -- te.
	Tie -- ne -- mi men -- te gio -- ia mia bel -- la guar -- dam' un poc' a me,
	a me, a me, a me, fa mi con -- tien -- to!
}
verseThree = \lyricmode {
  % Liedtext folgt hier.
	\set stanza = "3."
	O cuo -- re man -- za mi -- a,
	o cuo -- re man -- za mia per -- fi -- do cuo -- re.
	Tu sei la gio -- ia mi -- a, tu sei la gio -- ia mi -- a, lo mio a~mo -- re.
}


altoVoice = \relative c' {
  \global
  \dynamicUp
  % Die Noten folgen hier.
  \repeat volta 2 {f2 c4 d d f f2 f r4 f e d d f f2. es4 es c c2 c1}
  c2 g'4 g f c c8( d e f g2) g r4 f f f f f f2 c4 f2 e4 g2 g
  \repeat volta 2 {f4 f8 f a4 a d, d8 d g4 g c, c8 c es4 c c c a2 r4 b a2 r4 b b c d2 d c1 c \fermata}
}

tenorVoice = \relative c' {
  \global
  \dynamicUp
  % Die Noten folgen hier.
  \repeat volta 2 {c2 a4 a b c d2 c r4 c c a b c d2. c4 b a g2 a1}
  f'2 d4 e c  f e( d8 c d2) e r4 c c c d d c2 a4 c2 c4 d2 e
  \repeat volta 2 {c4 c8 c d4 d h h8 h c4 c a a8 a b4 a g2 r4 f f2 r4 f f2 g a b g1 a \fermata}
}

bassVoice = \relative c {
  \global
  \dynamicUp
  % Die Noten folgen hier.
  \repeat volta 2 {f2 f4 d g f b,2 f' r4 f c d g f b,2. c4 es f c2 f1}
  f2 g4 c, f2 a g c, r4 f f f  b b, f'2 f4 f2 a4 g2 c,
  \repeat volta 2 {f4 f8 f d4 d g g8 g c,4 c f f8 f es4 f c2 r4 d b2 r4 d b2 es d b c1 f \fermata}
}

sopranoVoicePart = \new Staff \with {
  instrumentName = "Sopran"
  midiInstrument = "choir aahs"
} { \sopranoVoice }
\addlyrics { \verseOne }
\addlyrics { \verseTwo }
\addlyrics { \verseThree }

altoVoicePart = \new Staff \with {
  instrumentName = "Alt"
  midiInstrument = "choir aahs"
} { \altoVoice }
\addlyrics { \verseOne }
\addlyrics { \verseTwoAlto }
\addlyrics { \verseThree }

tenorVoicePart = \new Staff \with {
  instrumentName = "Tenor"
  midiInstrument = "choir aahs"
} { \clef "treble_8" \tenorVoice }
\addlyrics { \verseOne }
\addlyrics { \verseTwo }
\addlyrics { \verseThree }

bassVoicePart = \new Staff \with {
  instrumentName = "Bass"
  midiInstrument = "choir aahs"
} { \clef bass \bassVoice }
\addlyrics { \verseOne }
\addlyrics { \verseTwo }
\addlyrics { \verseThree }

\score {
	\new ChoirStaff {
	  <<
	    \sopranoVoicePart
	    \altoVoicePart
	    \tenorVoicePart
	    \bassVoicePart
	  >>
	}
	\layout { }
}
\score {
	\unfoldRepeats
	<<
	    \sopranoVoicePart
	    \altoVoicePart
	    \tenorVoicePart
	    \bassVoicePart
	>>
	\midi {
		\tempo 4=100
	}
}