
\version "2.18.2"

%% additional definitions required by the score:
\language "deutsch"

#(set-global-staff-size 18.7)

\paper {
	#(set-paper-size "a4")
	indent = 0
	system-system-spacing #'basic-distance = #24
}

\header {
	title = "Es fiel ein Reif"
	composer = "Felix Mendelssohn Bartholdy"
	opus = "Opus 41, Nr. 3"
	subtitle = "aus \"Drei Volkslieder\""
	poet = "Text von Heinrich Heine"
	copyright = "© Volker Seibt 2018 (Lizenz CC BY-SA 3.0 DE)"
	% Voreingestellte LilyPond-Tagline entfernen
	tagline = ##f
}
%
\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

SopranStimme = \new Voice = "sopran" \relative c' {
	\voiceOne
    \clef "treble" \key c \major \time 4/4
    \partial 4
    \repeat volta 2 {
    	e4 ^"Un poco Allegro" ^\p -. |
		c'4-. h4 -. a4 -. gis8. a16 | % 1
	    h4 -. e,4 -. a4 -. e4 -. | % 2
		\phrasingSlurDashed
	    a4 -. gis8. a16 h4 -. e,8 -. \( e8-. \) | % 3
	    a4 -. gis8. a16 h4 -. e,4 | % 4
	    c'2 ^\markup{ \italic {cresc.} } d2 | % 5
	    e4. ^\f (d8) c4 a8 ^\p  h8 | % 6
	    c4 h8 a8 gis2 | % 7
		a2.
	}
	% Strophe 3
	e4-.
	c'4-. h4 -. a4 -. gis8. (a16) |
    h4 -. e,4 -. a4 -. e4 -. | % 2
	\phrasingSlurDashed
    a4 -. gis8. a16 h4 -. e,8 -. e8-. | % 3
    a4 -. gis8. a16 h4 -. e,4 | % 4
    c'2 ^\markup{ \italic {cresc.} } d2 | % 5
    e4. ^\f (d8) c4 a8 ^\p h8 | % 6
    c4 h8 a8 gis2 | % 7
    a2. \fermata \bar "|."
}

AltStimme = \new Voice = "alt" \relative c' {
	\voiceTwo
    \clef "treble" \key c \major \time 4/4
    \partial 4
    \repeat volta 2 {
	    e4 -. |
	    e4 -. e4 -. e4 -. e8. e16 | % 1
	    e4 -. e4 -. e4 -. e4 -. | % 2
	    e4 -. e8. a16 gis4 -. e8-. e-. | % 3
	    e4 -. e8. a16 gis4 -. e4 | % 4
	    a2 _\markup{ \italic {cresc.} }  h2 | % 5
	    c2 \f e,4 dis4 \p | % 6
	    e1 | % 7
		e2.
	}
	% Strophe 3
	e4-.^\p |
	e4-. e-. a-. e-. |
    f-. e-. e-. e4 -. |
    e4 -. e8. a16 gis4 -. e8-. e-. |
    e4 -. e8. a16 gis4 -. e4 | % 4
    a2 _\markup{ \italic {cresc.} }  h2 | % 5
    c2 \f e,4 dis4 \p | % 6
    e1 | % 7
	e2.\fermata \bar "|."
}

LyricsStaffEinsStropheEins = \new Lyrics \lyricsto "sopran" {
	\set stanza = "1."
	Es fiel ein Reif in der Früh -- lings -- nacht,
	er fiel auf die bun -- ten __ _ Blau -- blü -- me -- lein,
	sie sind ver -- wel -- ket,
	ver -- _ wel -- ket, ver -- dor -- ret.
	\set stanza = "3."
	Sie sind ge -- wan -- dert hin und her,
	sie ha -- ben ge -- habt we -- der Glück noch __ _ Stern,
	sie sind ge -- stor -- ben,
	ge -- _ stor -- ben, ver -- dor -- ben.
}

LyricsStaffEinsStropheZwei = \new Lyrics \lyricsto "sopran" {
	\set stanza = "2."
    Ein Jüng -- ling hat -- te ein Mäd -- chen lieb,
    sie flo -- hen __ _ heim -- lich von Hau -- se __ _ fort,
    es wusst' weder Va -- ter,
    we -- der Va -- ter noch Mut -- ter.
}

TenorStimme = \new Voice = "tenor" \relative c {
	\voiceOne
    \clef "bass" \key c \major \time 4/4
    \partial 4
    \repeat volta 2 {
	    e4 -. |
	    e'4 -. d4 -. c4 -. h8. c16 | % 1
	    d4 -. h4 -. c4 -. e4 -. | % 2
	    c4 -. h8. a16 e'4 -. e4 -. | % 3
	    c4 -. h8. a16 e'4 -. e4 | % 4
	    e2 ^\markup{ \italic {cresc.} }  f2 | % 5
	    g4. ^\f ( f8 ) e4 a,4 ^\p | % 6
	    a4 h8 c8 h4 ( e4 ) | % 7
		e2.
    }
    e,4-.
    e'-. d-. c-. a-. | % 1
    d-. d-. c-. e4 -. | % 2
    c4 -. h8. a16 e'4 -. e4 -. | % 3
    c4 -. h8. a16 e'4 -. e4 | % 4
    e2 ^\markup{ \italic {cresc.} }  f2 | % 5
    g4. ^\f ( f8 ) e4 a,4 ^\p | % 6
    a4 h8 c8 h4 ( e4 ) | % 7

    e2.\fermata \bar "|."
}

BassStimme = \new Voice = "bass" \relative {
	\voiceTwo
    \clef "bass" \key c \major \time 4/4
    \partial 4
    \repeat volta 2 {
	    e4 \p -. |
	    a4 -. a4 -. a4 -. a8. a16 | % 3
	    e4 -. e4 -. a,4 -. r4 | % 4
	    R1 | % 5
	    r2 r4 e'4 | % 6
	    a2 _\markup{ \italic {cresc.} }
	    \once \override TextScript.padding = 4.5 g2 _\markup { \italic {Alt und Baß:}} | % 7
	    c,2 \f c4 f4 \p | % 8
	    e1 | % 9
		a,2.
    }
    e'4\p-. |
	a4-. e-. f-. c-. | % 1
    d-. e-. a,-. r4 | % 4
    R1 | % 5
    r2 r4 e'4 | % 6
    a2 _\markup{ \italic {cresc.} }
    \once \override TextScript.padding = 4.2 g2
    _\markup { \italic {Alt und Baß:}} | % 7
    c,2 \f
    c4 f4\p | % 8
    e1 | % 9
    a,2.\fermata \bar "|."
} % \override TextScript.padding = 9

LyricsStaffZweiStropheEins = \new Lyrics \lyricsto "bass" {
	\skip4 _ _ _ _ _ _ _ _ _
	_ _ _ _ ver -- dor -- ret.
	% Strophe 3
	\skip4 _ _ _ _ _ _ _ _ _
	_ _ _ ver -- dor -- ben.
}

LyricsStaffZweiStropheZwei = \new Lyrics \lyricsto "bass" {
	_ _ _ _ _ _ _ _ _ _
	_ _ _ _ noch Mut -- ter. _
}

LyricsStaffZweiStropheDrei = \new Lyrics \lyricsto "bass" {
	_ _ _ _ _ _ _ _ _ _
	_ _ _ _ ver -- dor -- ben.
}

\score {
	\new ChoirStaff {
		<<
			\new Staff
			<<
				\SopranStimme
				\AltStimme
			>>
			\LyricsStaffEinsStropheEins
			\LyricsStaffEinsStropheZwei

			\new Staff
			<<
				\TenorStimme
				\BassStimme
			>>
			\LyricsStaffZweiStropheEins
			\LyricsStaffZweiStropheZwei
		>>
	}
	\layout {}
	\midi {}
}
